# appdocker
Este es un repositorio para crear una imagen docker.

Creacion de imagen.

```
$ git clone https://gitlab.com/sergio.pernas2/app-docker.git
$ cd app-docker
$ sudo docker build -t app-docker .
```

Despliegue.

```
$ sudo docker run -d --name app-docker -p 9050:80 app-docker
```